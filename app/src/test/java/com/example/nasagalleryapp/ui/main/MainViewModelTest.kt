package com.example.nasagalleryapp.ui.main

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.nasagalleryapp.ui.model.NasaImageModel
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
internal class MainViewModelTest {

    @Test
    fun getJsonDataFromAsset() {
        val viewModel = MainViewModel()
        viewModel.getJsonDataFromAsset(ApplicationProvider.getApplicationContext())

        assertEquals(viewModel.mImageLiveData.value, "foo")

    }
}