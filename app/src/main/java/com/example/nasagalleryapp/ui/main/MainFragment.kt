package com.example.nasagalleryapp.ui.main

import android.content.Context
import android.graphics.Rect
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nasagalleryapp.MainActivity
import com.example.nasagalleryapp.R
import com.example.nasagalleryapp.databinding.FragmentMainBinding
import com.example.nasagalleryapp.ui.adapter.CustomAdapter
import com.example.nasagalleryapp.ui.model.NasaImageModel
import com.example.nasagalleryapp.ui.utils.AppConstant

class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel

    private lateinit var mFragmentMainBinding : FragmentMainBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mFragmentMainBinding = FragmentMainBinding.inflate(inflater,container,false)
        return mFragmentMainBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        (activity as MainActivity)?.setSupportActionBar(mFragmentMainBinding.tbMain.toolbar)
        (activity as MainActivity)?.supportActionBar?.title = getString(R.string.app_name)
        setupRecyclerView()
        viewModel.getJsonDataFromAsset(requireContext())
    }

    private fun setupRecyclerView() {

        viewModel.mImageLiveData.observe(viewLifecycleOwner, Observer {
            if(!it.isNullOrEmpty()) {
                val mainAdapter =
                    CustomAdapter(it, object : CustomAdapter.OnNasaImageClickListener {
                        override fun onNasaImageClick(position: Int) {
                            val bundle =
                                bundleOf(AppConstant.CONST_ARG_NASA_IMAGE to it[position])
                            Navigation.findNavController(mFragmentMainBinding.root)
                                .navigate(R.id.action_mainFragment_to_imageDetailFragment, bundle)
                        }
                    })

                mFragmentMainBinding.gridImages.layoutManager =
                    GridLayoutManager(requireContext(), 3)
                mFragmentMainBinding.gridImages.adapter = mainAdapter
                mFragmentMainBinding.gridImages.addItemDecoration(
                    ItemOffsetDecoration(
                        requireContext()
                    )
                )

                runLayoutAnimation()
            } else {
                Toast.makeText(requireContext(),"No Data Available!",Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun runLayoutAnimation() = mFragmentMainBinding.gridImages.apply {
        layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.grid_layout_animation_from_bottom)
        adapter?.notifyDataSetChanged()
        scheduleLayoutAnimation()
    }

    private class ItemOffsetDecoration(
        context: Context
    ) : RecyclerView.ItemDecoration() {
        private val spacing = 10
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) = outRect.set(spacing, spacing, spacing, spacing)
    }

}