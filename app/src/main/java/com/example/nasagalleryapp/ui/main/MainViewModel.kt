package com.example.nasagalleryapp.ui.main

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nasagalleryapp.ui.model.NasaImageModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.lifecycle.HiltViewModel
import java.io.IOException

@HiltViewModel
class MainViewModel : ViewModel() {

    val mImageLiveData = MutableLiveData<List<NasaImageModel>>()

    fun getJsonDataFromAsset(context: Context) {
        val imageData: List<NasaImageModel>
        try {
            val jsonString = context.assets.open("data.json").bufferedReader().use { it.readText() }
            val listPersonType = object : TypeToken<List<NasaImageModel>>() {}.type
            imageData = Gson().fromJson(jsonString, listPersonType)
            mImageLiveData.postValue(imageData)
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            mImageLiveData.postValue(mutableListOf())
        }
    }
}