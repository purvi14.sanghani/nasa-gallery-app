package com.example.nasagalleryapp.ui.splash

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.nasagalleryapp.R
import com.example.nasagalleryapp.databinding.FragmentSplashBinding
import kotlinx.coroutines.*

class SplashFragment : Fragment() {
    private lateinit var  _binding: FragmentSplashBinding
    val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSplashBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activityScope.launch {
            delay(3000)
            Navigation.findNavController(_binding.root).navigate(R.id.action_splashFragment_to_mainFragment)
        }
    }

    override fun onPause() {
        super.onPause()
    }
}