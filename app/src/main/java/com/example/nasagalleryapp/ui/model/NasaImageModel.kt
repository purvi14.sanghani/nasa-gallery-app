package com.example.nasagalleryapp.ui.model

import android.os.Parcel
import android.os.Parcelable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import java.io.Serializable

data class NasaImageModel(
  var copyRight: String = "",
  var date: String = "",
  var explanation: String = "",
  var hdurl: String = "",
  var media_type: String = "",
  var service_version: String = "",
  var title: String = "",
  var url: String = "",
  ) : Parcelable {
  constructor(parcel: Parcel) : this(
    parcel.readString()!!,
    parcel.readString()!!,
    parcel.readString()!!,
    parcel.readString()!!,
    parcel.readString()!!,
    parcel.readString()!!,
    parcel.readString()!!,
    parcel.readString()!!
  ) {
  }

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(copyRight)
    parcel.writeString(date)
    parcel.writeString(explanation)
    parcel.writeString(hdurl)
    parcel.writeString(media_type)
    parcel.writeString(service_version)
    parcel.writeString(title)
    parcel.writeString(url)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<NasaImageModel> {

    @JvmStatic // add
    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView, url: String?) {
      if (!url.isNullOrEmpty()) {
        Glide.with(view.context).load(url).into(view)
      }
    }

    override fun createFromParcel(parcel: Parcel): NasaImageModel {
      return NasaImageModel(parcel)
    }

    override fun newArray(size: Int): Array<NasaImageModel?> {
      return arrayOfNulls(size)
    }
  }
}