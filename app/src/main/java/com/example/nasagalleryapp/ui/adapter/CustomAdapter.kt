package com.example.nasagalleryapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nasagalleryapp.databinding.RowNasaImageCardBinding
import com.example.nasagalleryapp.ui.model.NasaImageModel

class CustomAdapter(private val mNasaImageList: List<NasaImageModel>,
                  private val onNasaImageClickListener:OnNasaImageClickListener)
    : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    interface OnNasaImageClickListener {
        fun onNasaImageClick(position: Int)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowNasaImageCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mNasaImageList[position])

        holder.itemView.setOnClickListener {
            onNasaImageClickListener.onNasaImageClick(position)
        }
    }

    override fun getItemCount(): Int {
        return mNasaImageList.size
    }

    class ViewHolder(private val nasaImageCardBinding: RowNasaImageCardBinding) : RecyclerView.ViewHolder(nasaImageCardBinding.root) {
       fun bind(nasaImageModel: NasaImageModel) {
           nasaImageCardBinding.nasaImageModel = nasaImageModel
       }
    }
}