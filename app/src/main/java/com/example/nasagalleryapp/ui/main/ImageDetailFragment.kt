package com.example.nasagalleryapp.ui.main

import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.nasagalleryapp.MainActivity
import com.example.nasagalleryapp.databinding.FragmentImageDetailBinding
import com.example.nasagalleryapp.ui.model.NasaImageModel
import com.example.nasagalleryapp.ui.utils.AppConstant


class ImageDetailFragment : Fragment() {

    lateinit var mFragmentImageDetailBinding : FragmentImageDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mFragmentImageDetailBinding = FragmentImageDetailBinding.inflate(inflater,container,false)
        return mFragmentImageDetailBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        val data = arguments?.getParcelable<NasaImageModel>(AppConstant.CONST_ARG_NASA_IMAGE)
        (activity as MainActivity)?.setSupportActionBar(mFragmentImageDetailBinding.tbDetail.toolbar)
        (activity as MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity)?.supportActionBar?.setDisplayShowHomeEnabled(true)
        if(data !=null) {
            (activity as MainActivity)?.supportActionBar?.title = data.title
            mFragmentImageDetailBinding.txtTitleDetailView.text = data.title
            mFragmentImageDetailBinding.txtSubTitleDetailView.text = data.explanation
            Glide.with(mFragmentImageDetailBinding.imageDetailView).load(data.hdurl).into(mFragmentImageDetailBinding.imageDetailView)
            mFragmentImageDetailBinding.imageDetailView
        } else {
            Navigation.findNavController(mFragmentImageDetailBinding.root).popBackStack()
        }
    }


}